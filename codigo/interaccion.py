#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Proveer una interface para la interacción con el usuario, que permita
manejar tanto interacciones exitosas como interacciones que se cancelan.
"""

INTERACCION_ESTADO_CANCELADA = 0
INTERACCION_ESTADO_OK = 1
ESTADO = 0
VALOR = 1


def _estado(resultado_interaccion):
    """Devolver INTERACCIÓN_ESTADO_OK si la interacción fue exitosa,
    e INTERACCIÓN_ESTADO_CANCELADA si la interacción se canceló.

    :param resultado_interaccion: Es el valor devuelto por las funciones
    'ok' y 'cancel'
    """
    return resultado_interaccion[ESTADO]


def ok(valor):
    """Devolver una valor que indique que la interacción fue exitosa y que
    permita acceder al resultado de la interacción.

    """
    return (INTERACCION_ESTADO_OK, valor)


def cancelar():
    """Devolver un valor que indique que la interacción se canceló."""
    return (INTERACCION_ESTADO_CANCELADA,)


def valor(resultado_interaccion):
    """Devolver el resultado de la interacción.

    Se debería usar cuando es_ok(resultado_interacción) == True.

    :param resultado_interaccion: Es el valor devuelto por las funciones
    'ok' y 'cancel'
    """

    return resultado_interaccion[VALOR]


def es_ok(resultado_interaccion):
    """"Devolver True si la interacción fue exitoda, sino devolver False"""

    return _estado(resultado_interaccion) == INTERACCION_ESTADO_OK


def es_cancelado(prompt_result):
    """Devolver True si la interacción se canceló, sino devolver False"""

    return _estado(prompt_result) == INTERACCION_ESTADO_CANCELADA


