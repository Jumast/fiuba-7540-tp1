#!/usr/bin/env python
# -*- coding: utf-8 -*-


def fila(coordenada):
    """Devolver el número de fila de la coordenada"""

    return coordenada[0]


def columna(coordenada):
    """Devolver el número de columna de la coordenada."""

    return coordenada[1]


def misma_fila(coordenada1, coordenada2):
    """Devolver True si las coordenadas están en la misma fila; caso
    contrario devolver False"""

    return fila(coordenada1) == fila(coordenada2)


def misma_columna(coordenada1, coordenada2):
    """Devolver True si las coordenadas están en la misma columna; caso
    contrario devolver False"""

    return columna(coordenada1) == columna(coordenada2)
