#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Proveer una interface para crear y manipular un tablero rectangular de letras.
"""

import string
import random
import coordenada as coordenada_


def _letra_aleatoria_minuscula():
    """Devolver una letra minúscula al azar."""
    return random.choice(string.ascii_lowercase)


def tablero_aleatorio(cant_filas, cant_columnas):
    """Generar un tablero rectangular de letras aleatorias minúsculas.

    :param cant_filas (int): Cantidad de filas del tablero que genera.
    :param cant_columnas (int): Cantidad de columnas del tablero que genera.
    :return: Lista de listas, donde cada lista interna tiene
    @cant_columnas letras aleatorias minúsculas.


    Ejemplo:

    >> tablero = tablero_aleatorio(3, 3)
    >> print tablero
    [['d', 'a', 'k'], ['p', 'i', 'h'], ['n', 'b', 'r']]
    """

    resultado = []
    for num_fil in range(1, cant_filas + 1):
        fila = []
        for num_col in range(1, cant_columnas + 1):
            fila.append(_letra_aleatoria_minuscula())
        resultado.append(fila)
    return resultado


def cant_filas(tablero):
    """ Devolver la cantidad de filas del tablero.

    :return (int):
    """

    return len(tablero)


def cant_columnas(tablero):
    """Devolver la cantidad de columnas del tablero.

    :return (int):
    """

    return len(tablero[0])


def contiene_coordenada(tablero, coordenada):
    """Decidir si <coordenada> está dentro de los límites del tablero."""

    return (coordenada_.columna(coordenada) <= cant_columnas(tablero) and
            coordenada_.fila(coordenada) <= cant_filas(tablero))


def contiene_coordenadas(tablero, coordenadas):
    contiene = True
    for coordenada in coordenadas:
        if not contiene_coordenada(tablero, coordenada):
            contiene = False
            break

    return contiene


def mostrar_tablero(tablero):
    """Imprime el tablero en pantalla.

    :param tablero: cant_columnas(<tablero>) <= 26.
    :return: None

    Ejemplo:

    >> tablero = tablero_aleatorio(3, 3)
    >> print tablero
    [['d', 'a', 'k'], ['p', 'i', 'h'], ['n', 'b', 'r']]

    >> mostrar_tablero(tablero)

        A   B   C
    1 | d   a   k
    2 | p   i   h
    3 | n   b   r
    """

    header = " " + " " + " " + " "
    for num in range(65, 65 + cant_columnas(tablero)):
        header += chr(num) + " "
    print header

    fila_num = 1
    for fila in tablero:

        linea_a_mostrar = str(fila_num)
        if fila_num < 10:
            linea_a_mostrar += " " + " |"
        else:
            linea_a_mostrar += " |"
        for letra in fila:
            linea_a_mostrar += letra + " "
        print linea_a_mostrar
        fila_num += 1


def obtener_letra(tablero, num_fil, num_col):
    """Devolver la letra de <tablero> que está en la posición <num_fil>,
    <num_col>

    :param num_fil: 1,2,...n
    :param num_col: 1,2,...n
    :return (str):
    """

    return tablero[num_fil - 1][num_col - 1]


def colocar_letra(letra, tablero, num_fil, num_col):
    """Poner <letra> en la posición <num_fil>,<num_col> de <tablero>.

    :param letra (str):
    :param num_fil (int): 1,2,...n
    :param num_col (int): 1,2,...n
    :return: None
    """
    tablero[num_fil - 1][num_col - 1] = letra


def obtener_fila(tablero, num_fil):
    """Devolver la fila <num_fil> de <tablero>

    :param num_fil (int): 1,2,...n
    :return (list): Fila (lista de letras) <num_fil> de <tablero>
    """

    return tablero[num_fil - 1]


def obtener_columna(tablero, num_col):
    """Devolver la columna <num_col> de <tablero>

    :param num_col: 1,2,...n
    :return (list): Columna (lista de letras) <num_col> de <tablero>
    """

    resultado = []
    for num_fil in range(1, cant_filas(tablero) + 1):
        letra = obtener_letra(tablero, num_fil, num_col)
        resultado.append(letra)

    return resultado


def colocar_palabra_horizontal(palabra, tablero, num_fil, num_col):
    """Colocar <palabra> en posición horizontal en la fila <num_fil> de
    <tablero>, poniendo la primera letra en la columna <num_col>.

    :param palabra (str):
    :param num_fil (int): 1,2,...n
    :param num_col (int): 1,2,...n
    :return: None
    """

    """
    fila = obtener_fila(tablero, num_fila)
    for indice_letra in range(num_col, len(palabra) + 1):
        fila[indice_letra + num_col] = palabra[indice_letra]
    """
    num_columna = num_col
    for letra in palabra:
        colocar_letra(letra, tablero, num_fil, num_columna)
        num_columna += 1


def colocar_palabra_vertical(palabra, tablero, num_col, num_fil):
    """Colocar <palabra> en posición vertical en la columna <num_col> de
    <tablero>, poniendo la primera letra en la fila <num_fil>.

    :param num_col (int): 1,2,...n
    :param num_fil (int): 1,2,...n
    :return: None
    """

    for indice_letra in range(0, len(palabra)):
        letra = palabra[indice_letra]
        num_fila = num_fil + indice_letra
        colocar_letra(letra, tablero, num_fila, num_col)


def transfomar_letras(tablero, coordenada1, coordenada2, funcion):
    """

    :param tablero:
    :param coordenada1:
    :param coordenada2:
    :return:
    """

    if coordenada_.misma_columna(coordenada1, coordenada2):
        columna = coordenada_.columna(coordenada1)
        fila1 = coordenada_.fila(coordenada1)
        fila2 = coordenada_.fila(coordenada2)

        if (fila1 <= fila2):
            rango = range(fila1, fila2 + 1)
        else:
            rango = range(fila1, fila2, -1)

        for fila in rango:
            letra = obtener_letra(tablero, fila, columna)
            letra_transformada = funcion(letra)
            colocar_letra(letra_transformada, tablero, fila, columna)

    elif coordenada_.misma_fila(coordenada1, coordenada2):
        fila = coordenada_.fila(coordenada1)
        col1 = coordenada_.columna(coordenada1)
        col2 = coordenada_.columna(coordenada2)

        if (col1 <= col2):
            rango = range(col1, col2 + 1)
        else:
            rango = range(col1, col2, -1)

        for columna in rango:
            letra = obtener_letra(tablero, fila, columna)
            letra_transformada = funcion(letra)
            colocar_letra(letra_transformada, tablero, fila, columna)


def obtener_letras_horizontal(tablero, num_fil, col_inicial, col_final):
    """Devolver una lista con las letras de la fila <num_fil> que estén
    entre (e incluyendo) las columnas <col_inicial> y <col_final>.
    Contemplar la posibilidad de ir tanto de derecha a izquierda como de
    izquierda a derecha.

    :param num_fil (int): 1,2,..cant_filas(tablero)
    :param col_inicial (int): 1,2,..cant_columnas(tablero)
    :param col_final (int: 1,2,..cant_columnas(tablero)
    :return (list): Lista de letras.
    """

    letras = []

    if (col_inicial <= col_final):
        rango = range(col_inicial, col_final + 1)
    else:
        # en este caso col_inicial > col_final
        rango = range(col_inicial, col_final - 1, -1)

    for num_col in rango:
        letras.append(obtener_letra(tablero, num_fil, num_col))

    return letras


def obtener_letras_vertical(tablero, num_col, fila_inicial, fila_final):
    """Devolver una lista con las letras de la fila <num_fil> que estén
    entre (e incluyendo) las columnas <col_inicial> y <col_final>

    :param num_fil (int): 1,2,..cant_filas(tablero)
    :param col_inicial (int): 1,2,..cant_columnas(tablero)
    :param col_final (int: 1,2,..cant_columnas(tablero)
    :return (list): Lista de letras.
    """

    letras = []

    if (fila_inicial <= fila_final):
        rango = range(fila_inicial, fila_final + 1)
    else:
        # en este caso fila_inicial > fila_final
        rango = range(fila_inicial, fila_final - 1, -1)

    for num_fil in rango:
        letras.append(obtener_letra(tablero, num_fil, num_col))

    return letras


def obtener_letras(tablero, coordenada_inicial, coordenada_final):
    """Devolver las letras del tablero entre <coordenada_inicial> y
    <coordenada_final>. Las coordenadas deben estar en la misma fila o
    columna.

    """

    # letras = []
    if coordenada_.misma_fila(coordenada_inicial, coordenada_final):
        num_fil = coordenada_.fila(coordenada_inicial)
        col_inicial = coordenada_.columna(coordenada_inicial)
        col_final = coordenada_.columna(coordenada_final)
        letras = obtener_letras_horizontal(tablero, num_fil, col_inicial, col_final)
    elif coordenada_.misma_columna(coordenada_inicial, coordenada_final):
        num_col = coordenada_.columna(coordenada_inicial)
        fila_inicial = coordenada_.fila(coordenada_inicial)
        fila_final = coordenada_.fila(coordenada_final)
        letras = obtener_letras_vertical(tablero, num_col, fila_inicial, fila_final)

    return letras