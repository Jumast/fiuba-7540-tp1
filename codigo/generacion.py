#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tablero as tablero_
import ubicacion_palabra as ubicacion_palabra_
import sopadeletras

INDICE_PALABRA_1 = 0
INDICE_PALABRA_2 = 1


def generar_sopa_de_letras(tamano_del_tablero, palabras):
    """Devolver el tablero inicial con el que se va a comenzar el juego,
    y las palabras que se van a utilizar con sus respectivas posiciones en
    el tablero.

    """

    CANT_FIL = tamano_del_tablero
    CANT_COL = tamano_del_tablero

    tablero = tablero_.tablero_aleatorio(CANT_FIL, CANT_COL)
    ubicacion_palabras = {}

    ubicacion_p1 = _colocar_primera_palabra(tablero, palabras)
    palabra = ubicacion_palabra_.palabra(ubicacion_p1)
    coord1 = ubicacion_palabra_.coord_inicial(ubicacion_p1)
    coord2 = ubicacion_palabra_.coor_final(ubicacion_p1)
    ubicacion_palabras[palabra] = (coord1, coord2)

    if(len(palabras) >= 2):
        ubicacion_p2 = _colocar_segunda_palabra(tablero, palabras)
        palabra = ubicacion_palabra_.palabra(ubicacion_p2)
        coord1 = ubicacion_palabra_.coord_inicial(ubicacion_p2)
        coord2 = ubicacion_palabra_.coor_final(ubicacion_p2)
        ubicacion_palabras[palabra] = (coord1, coord2)

    if(len(palabras) >= 3):
        r3 = _colocar_palabras_restantes(tablero, palabras)
        for ubicacion in r3:
            palabra = ubicacion_palabra_.palabra(ubicacion)
            coord1 = ubicacion_palabra_.coord_inicial(ubicacion)
            coord2 = ubicacion_palabra_.coor_final(ubicacion)
            ubicacion_palabras[palabra] = (coord1, coord2)

    return sopadeletras.crear(tablero, ubicacion_palabras)


def _colocar_primera_palabra(tablero, palabras):
    """Colocar <palabra> al derecho y en vertical, en la primera columna de
    <tablero>, comenzando con la primera letra en la primera fila. Devolver
    la palabra colocada y su ubicación en el tablero.

    :return: None
    """

    num_fil = 1
    num_col = 1
    primera_palabra = palabras[0]

    tablero_.colocar_palabra_vertical(primera_palabra, tablero, num_col,
                                      num_fil)

    return ubicacion_palabra_.crear(palabras[0], (num_fil, num_col),
                                    (len(palabras[0]), num_col))


def _colocar_segunda_palabra(tablero, palabras):
    """Colocar <palabra> invertida y en horizontal, en la fila
    <long_primera_palabra> + 1, con la primera letra en la primera columna.
    Devolver la palabra colocada y su ubicación en el tablero.

    :param long_primera_palabra (int): Longitud de la primera palabra que se
    colocó con _colocar_primera_palabra.
    :return:
    """

    long_primera_palabra = len(palabras[INDICE_PALABRA_1])
    palabra_invertida = palabras[1][-1::-1]
    num_fil = long_primera_palabra + 1
    num_col_primera_letra = 1
    tablero_.colocar_palabra_horizontal(palabra_invertida, tablero, num_fil,
                                        num_col_primera_letra)

    coordenada_1 = num_fil, num_col_primera_letra

    """
    la segunda palabra se coloca horizontalmente, comenzando con la primera
    letra en la primera columna; de esta manera, el número de columna de la
    última letra coincide con la longitud de la palabra.
    """
    num_col_ultima_letra = len(palabras[INDICE_PALABRA_2])
    coordenada_2 = num_fil, num_col_ultima_letra

    ubicacion_palabra = coordenada_1, coordenada_2
    return palabras[INDICE_PALABRA_2], ubicacion_palabra


def _colocar_palabras_restantes(tablero, palabras):
    """Colocar desde la tercera palabra en adelante, una por fila, al derecho.
    Devolver las palabras colocadas y sus posiciones en el tablero.

    """

    # ya se colocaron dos palabras
    palabras_restantes = palabras[2:]

    filas_ocupadas_palabra_1 = len(palabras[INDICE_PALABRA_1])
    filas_ocupadas_palabra_2 = 1
    num_fil = filas_ocupadas_palabra_1 + filas_ocupadas_palabra_2 + 1
    num_columna_primera_letra = 1
    resultado = []
    for palabra in palabras_restantes:
        tablero_.colocar_palabra_horizontal(palabra, tablero, num_fil,
                                            num_columna_primera_letra)

        coord1 = (num_fil, num_columna_primera_letra)

        num_columna_ultima_letra = len(palabra)
        coord2 = (num_fil, num_columna_ultima_letra)

        ubicacion = ubicacion_palabra_.crear(palabra, coord1, coord2)
        resultado.append(ubicacion)

        num_fil += 1

    return resultado









