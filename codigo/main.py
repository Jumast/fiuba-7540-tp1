#!/usr/bin/env python
# -*- coding: utf-8 -*-

import interaccion
import configuracion
import generacion
import juego


def main():
    interaccion_configuracion = configuracion.solicitar_tamannio_y_palabaras()

    if interaccion.es_cancelado(interaccion_configuracion):
        return

    tamannio_y_palabaras = interaccion.valor(interaccion_configuracion)
    tamano_del_tablero = configuracion.tamano_del_tablero(tamannio_y_palabaras)
    palabras = configuracion.palabras(tamannio_y_palabaras)
    sopadeletras = generacion.generar_sopa_de_letras(tamano_del_tablero, palabras)
    juego.jugar(sopadeletras)


main()

