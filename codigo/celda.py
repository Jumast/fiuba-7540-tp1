#!/usr/bin/env python
# -*- coding: utf-8 -*-

import string


def letra_a_numero_columna(letra):
    """Convertir <letra> a número de columna. Da lo mismo ingresar
    mayúasculas o minúsculas.

    :letra (str): A,B,...,Z,a,b,...z
    :return (int):

    Ejemplo:

    >> letra_a_numero_columna("A")
    1

    >> letra_a_numero_columna("b")
    2

    >> letra_a_numero_columna("Z")
    26
    """
    return (ord(letra.upper())) - (ord("A") - 1)


def representa_coordenada(cadena):
    """Decidir si <cadena> puede representar una coordenada de un tablero.

    Devolver True si la cadena es una cadena de n caracteres, donde los n-1
    primeros representan un número entero mayor que cero y el último es una
    letra de la A a la Z (mayúscula o minúscula); caso contrario devolver
    False.

    :param cadena (str):
    :return (bool):
    """

    parte_numerica = cadena[0:-1]
    parte_letra = cadena[-1]
    return ((parte_numerica.isdigit() and int(parte_numerica) > 0) and
            parte_letra in string.ascii_letters)


def a_coordenada(celda):
    """Convertir una posición dada en formato celda a una posición dada en
    fomato coordenada.

    :param celda (str):
    :return (tuple):

    Ejemplo:

    >> celda = "1A"
    >> celda_a_coordenada(celda)
    (1, 1)
    """

    return (int(celda[0:-1]), letra_a_numero_columna(celda[-1]))