#!/usr/bin/env python
# -*- coding: utf-8 -*-

PALABRA = 0
COORDENADAS = 1
COORDENADA_INICIAL = 0
COORDENADA_FINAL = 1

"""Proveer una interface que permita saber que posición del tablero le
corresponde a cada palabra"""


def crear(palabra, coordenada_inicial, coordenada_final):
    """Devolver una tupla de dos elementos, siendo el primer elemento una
    palabra que está en el tablero, y la segunda una tupla que tiene las
    coordenadas de la palabra en el tablero.

    :param palabra (str):
    :param coordenada_inicial (int):
    :param coordenada_final (int):

    """

    return palabra, (coordenada_inicial, coordenada_final)


def palabra(ubicacion_palabra):

    return ubicacion_palabra[PALABRA]


def coord_inicial(ubicacion_palabra):

    return ubicacion_palabra[COORDENADAS][COORDENADA_INICIAL]


def coor_final(ubicacion_palabra):

    return ubicacion_palabra[COORDENADAS][COORDENADA_FINAL]
