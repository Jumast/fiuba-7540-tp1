#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tablero as tablero_
import celda
import coordenada
import sopadeletras as sopadeletras_

SENTINELA = "*"
BREAK = "\n"


def jugar(sopadeletras):
    """Imprimir el estado del tablero y solicitar al usuario que encuentre
    la siguiente palabra hasta que se encuentren todas las palabras.

    """

    tablero = sopadeletras_.tablero(sopadeletras)
    posicion_palabras = sopadeletras_.posiciones_palabras(sopadeletras)

    mostrar_estado_tablero(tablero)

    palabras_encontradas = []

    while len(palabras_encontradas) < len(posicion_palabras.keys()):

        mensaje = ""
        mensaje += "Faltan encontrar {} palabra(s).".format(len(
            sopadeletras_.palabras(sopadeletras)) - len(palabras_encontradas))
        mensaje += BREAK
        mensaje += "Introduzca dos celdas < '{}' para cancelar >.".format(
            SENTINELA)
        mensaje += BREAK
        mensaje += ">>"

        user_input = raw_input(mensaje)

        if user_input == SENTINELA:
            print "Juego cancelado."
            return

        coordenadas = try_parse(user_input)
        if not coordenadas:
            print "No se reconoce el formato de celda ingresado. Inténtelo nuevamente."
            continue

        coord_inicial, coord_final = coordenadas
        if not tablero_.contiene_coordenadas(tablero, [coord_inicial, coord_final]):
            print "Alguna (o ambas) de las coordenas está fuera de rango."
            continue

        if not (coordenada.misma_fila(coord_inicial, coord_final) or coordenada.misma_columna(coord_inicial, coord_final)):
            print "Las coordenadas ingresadas no están en la misma fila o columna."
            continue

        posible_palabra = sopadeletras_.letras(sopadeletras, coord_inicial, coord_final)
        if not sopadeletras_.tiene_palabra(sopadeletras, posible_palabra, coord_inicial, coord_final):
            print "No se encontró palabra en la posición especificada."
            continue

        if posible_palabra in palabras_encontradas:
            print "'{}' ya encontrada.".format(posible_palabra)
            continue

        palabras_encontradas.append(posible_palabra)
        sopadeletras_.encontrado(sopadeletras,posible_palabra)
        mostrar_estado_tablero(tablero)

    print "Encontraste todas las palabras! Felicitaciones!"


def mostrar_estado_tablero(tablero):
    """Mostrar el estado del tablero."""

    print
    tablero_.mostrar_tablero(tablero)
    print


def try_parse(cadena):
    """Decidir si <cadena> constiuye una representación válida de dos
    coordenadas; en caso afirmativo devolver una tupla con las dos
    coordenadas, y en caso negativo devolver una tupla vacía.

    Ejemplo:

    >> cadena = "2D 5C"
    >> coordenadas = try_parse(cadena)
    >> coordenadas
    ((2,4), (5,3))

    """

    posibles_celdas = cadena.split()
    if len(posibles_celdas) == 2:
        celda1 = posibles_celdas[0]
        celda2 = posibles_celdas[1]
        if (celda.representa_coordenada(
                celda1) and celda.representa_coordenada(
                celda2)):
            return (celda.a_coordenada(celda1), celda.a_coordenada(celda2))
        else:
            return
    else:
        return







