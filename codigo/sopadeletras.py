#!/usr/bin/env python
# -*- coding: utf-8 -*-

TABLERO = 0
POSICIONES_PALABRAS = 1
COORDENADA_INICIAL_PALABRA = 0
COORDENADA_FINAL_PALABRA = 1

import tablero as tablero_


def posiciones_palabras(sopadeletras):
    """Devolver un diccionario con las palabras con las que se va a jugar y sus
    respectivas posiciones en el tablero.

    :param sopadeletras: Tupla de dos elementos:
    Primer elemento (list): Tablero con el que se va a jugar.
    Segundo elemento (dict): Diccionario con las palabras y sus posiciones en
    el tablero.
    :return: Diccionario con las palabras y sus posiciones en el tablero:
    Llave: palabra que se colocó en el tablero.
    Valor: Tupla con las coordenadas de la palabra en el tablero.
    Ejemplo:
    >> resultado = {
        'vaso': ((1,1), (4,1))
        'papel': ((6,1), (6,4))
        'hoja': ((5,1), (5,5))
    }

    """

    return sopadeletras[POSICIONES_PALABRAS]


def crear(tablero, ubicacion_palabras):
    """Devolver una tupla donde el primer elemento es el tablero con el que se
    va a jugar, y el segundo representa la ubicacion de las palabras en el
    tablero.

    :param tablero (list): Tablero con el que se va a jugar.
    :param ubicacion_palabras: Diccionario.
    Llave: palabra que se colocó en el tablero.
    Valor: Tupla con las coordenadas de la palabra en el tablero.
    Ejemplo:
    >> ubicacion_palabras = {
        'vaso': ((1,1), (4,1))
        'papel': ((6,1), (6,4))
        'hoja': ((5,1), (5,5))
    }
    :return (tuple):
    """
    return (tablero, ubicacion_palabras)


def tablero(sopadeletras):
    """Devolver el tablero con el que se va a jugar.

    :param sopadeletras: Tupla de dos elementos:
    Primer elemento (list): Tablero con el que se va a jugar.
    Segundo elemento (dict): Diccionario con las palabras y sus posiciones en
    el tablero.
    :return (list): Tablero con el que se va a jugar.
    """

    return sopadeletras[TABLERO]


def palabras(sopadeletras):
    """Devolver una lista con las palabras que se colocaron en el tablero.

    :param sopadeletras: Tupla de dos elementos:
    Primer elemento (list): Tablero con el que se va a jugar.
    Segundo elemento (dict): Diccionario con las palabras y sus posiciones en
    el tablero.
    """

    return posiciones_palabras(sopadeletras).keys()


def posicion(sopadeletras, palabra):
    """Devolver la posición en el tablero de la palabra pasada por parámetro.

    :param sopadeletras: Tupla de dos elementos:
    Primer elemento (list): Tablero con el que se va a jugar.
    Segundo elemento (dict): Diccionario con las palabras y sus posiciones en
    el tablero.
    :param palabra:
    :return (tuple): Tupla de dos elementos:
    Primer elemento: Primera coordenada de la palabra en el tablero.
    Segundo elemento: Segunda coordenada de la palabra en el tablero.
    """

    return sopadeletras[POSICIONES_PALABRAS][palabra]


def tiene_palabra(sopadeletras, palabra, coord_inicial, coord_final):
    """Devolver True si la palabra pasada por parámetro está en el tablero
    en la posición indicada por los parámetros <coord_inicial> y <coord_final>.

    :param sopadeletras: Tupla de dos elementos:
    Primer elemento (list): Tablero con el que se va a jugar.
    Segundo elemento (dict): Diccionario con las palabras y sus posiciones en
    el tablero.
    :return (bool):
    """

    tiene = palabra in palabras(sopadeletras)
    if tiene:
        pos = posicion(sopadeletras, palabra)
        return (pos[COORDENADA_INICIAL_PALABRA] == coord_inicial
                and pos[COORDENADA_FINAL_PALABRA] == coord_final)

    else:
        return False


def letras(sopadeletras, coord_inicial, coord_final):

    tablero_juego = tablero(sopadeletras)
    letras = tablero_.obtener_letras(tablero_juego, coord_inicial, coord_final)
    return "".join(letras).lower()


def encontrado(sopadeletras, palabra):

    tablero_juego = tablero(sopadeletras)
    posicion_palabra_en_tablero = posicion(sopadeletras, palabra)
    coord_inicial_palabra = posicion_palabra_en_tablero[COORDENADA_INICIAL_PALABRA]
    coord_final_palabra = posicion_palabra_en_tablero[COORDENADA_FINAL_PALABRA]
    tablero_.transfomar_letras(tablero_juego, coord_inicial_palabra, coord_final_palabra,lambda x: x.upper())
