#!/usr/bin/env python
# -*- coding: utf-8 -*-

import interaccion

TAMANO_DEL_TABLERO_MIN_VALOR = 10
TAMANO_DEL_TABLERO_MAX_VALOR = 20
SENTINELA = "*"
BREAK = "\n"
TITULO_JUEGO = "SOPA DE LETRAS"
INDICE_TAMANO_DEL_TABLERO = 0
INDICE_PALABRAS = 1
LONGITUD_MIN_PALABRA = 3


def solicitar_tamannio_y_palabaras():
    """ Solicitar por consola el tamaño del tablero, la cantidad de palabras,
    y las palabaras con las que se va a jugar. Se la interacción es exitosa,
    devolver interaccion.ok((tamano_del_tablero, palabras)); caso contrario,
    devolver interaccion.cancelar().

   """

    dar_la_bienvenida()

    prompt_tamano_del_tablero = interaccion_tamano_del_tablero()
    if interaccion.es_cancelado(prompt_tamano_del_tablero):
        return interaccion.cancelar()
    tamano_del_tablero = interaccion.valor(prompt_tamano_del_tablero)

    prompt_numero_de_palabras = interaccion_numero_de_palabras(
        tamano_del_tablero)
    if interaccion.es_cancelado(prompt_numero_de_palabras):
        return interaccion.cancelar()
    numero_de_palabras = interaccion.valor(prompt_numero_de_palabras)

    prompt_palabras = interaccion_palabras(tamano_del_tablero,
                                           numero_de_palabras)
    if interaccion.es_cancelado(prompt_palabras):
        return interaccion.cancelar()
    palabras = interaccion.valor(prompt_palabras)

    return interaccion.ok((tamano_del_tablero, palabras))


def dar_la_bienvenida():
    """Imprimir por pantalla un mensaje de bienvenida al juego."""
    print
    print TITULO_JUEGO

    subrayado = ""
    for letra in TITULO_JUEGO:
        subrayado += "="
    print subrayado

    print
    print "Bienvenido al juego!"
    print


def interaccion_tamano_del_tablero():
    """Solicitar por consola el tamaño del tablero, validando que sea un
    número entero entre TAMANO_DEL_TABLERO_MIN_VALOR y
    TAMANO_DEL_TABLERO_MAX_VALOR (ambos valores permitidos), hasta que se
    ingrese un valor válido o se cancele la interacción ingresando <sentinela>.
    Si el usuario cancela la interacción, devolver interaccion.cancelada();
    si la interacción es exitosa, devolver interaccion.ok(valor ingresado)

    """

    return interaccion_entero(TAMANO_DEL_TABLERO_MIN_VALOR, TAMANO_DEL_TABLERO_MAX_VALOR, "Ingrese tamano del tablero")


def interaccion_numero_de_palabras(tamano_del_tablero):
    """Solicitar por consola el número de palabras con el que se va a jugar,
    validando que el valor ingresado sea menor o igual que
    <tamaño_del_tablero> / 2, hasta que se ingrese un valor
    válido o se cancele la interacción ingresando <sentinela>.
    Si el usuario cancela la interacción, devolver interaccion.cancelada();
    si la interacción es exitosa, devolver interaccion.ok(valor ingresado)

    """

    mensaje = "Ingrese la cantidad de palabras con la que se jugará la partida"
    return interaccion_entero(1, tamano_del_tablero / 2, mensaje)


def interaccion_palabras(tamano_del_tablero, numero_de_palabras_requerido):
    """Solicitar por consola las palabras con las que se jugará la
    partida, considerando que:
    * Se deben ingresar <numero_de_palabras_requerido> palabras.
    * La longitud de cada palabra debe estar entre 3 y <tamano_del_tablero> / 2
    * Las palabras sólo pueden estar formadas por letras de la 'a' a la 'z'.
    * No se admiten palabras repetidas.

    """
    palabras_ingresadas_validas = []

    mensaje = "Ingrese una palabra < '{}' para cancelar >.".format(SENTINELA)
    mensaje += "\n"
    mensaje += "* Longitud de la palabra entre 3 y {}.".format(
        _max_longitud_de_palabra(tamano_del_tablero))
    mensaje += "\n"
    mensaje += "* Sólo se admiten letras de la 'a' a la 'z' o la 'A' a la 'Z'"
    mensaje += "\n"
    mensaje += ">>"

    print
    print "Ingreso de palabras."

    while len(palabras_ingresadas_validas) < numero_de_palabras_requerido:

        print
        input_usuario = raw_input(mensaje)

        if input_usuario == SENTINELA:
            return interaccion.cancelar()

        if (not _palabra_tiene_longitud_correcta(input_usuario, tamano_del_tablero) or (not _palabra_tiene_solo_caracteres_permitidos(input_usuario))):
            print ("'{}' no es un palabra válida. Restan {} palabras".format(input_usuario, _cant_palabras_restantes(numero_de_palabras_requerido, palabras_ingresadas_validas)))
            continue

        if input_usuario.lower() in palabras_ingresadas_validas:
            print ("'{}' ya fue ingresada. Restan {} palabras".format(input_usuario, _cant_palabras_restantes(numero_de_palabras_requerido, palabras_ingresadas_validas)))
            continue

        palabras_ingresadas_validas.append(input_usuario.lower())
        print ("'{0}' admitida. Restan {1} palabras.".format(input_usuario, _cant_palabras_restantes(numero_de_palabras_requerido, palabras_ingresadas_validas)))

    return interaccion.ok(palabras_ingresadas_validas)


def _max_longitud_de_palabra(tamannio_tablero):

    return tamannio_tablero / 2

def _palabra_tiene_longitud_correcta(palabra, tamannio_tablero):

    longitud_palabra = len(palabra)
    longitud_max_palabra = _max_longitud_de_palabra(tamannio_tablero)
    return LONGITUD_MIN_PALABRA <= longitud_palabra <= longitud_max_palabra

def _palabra_tiene_solo_caracteres_permitidos(palabra):

    return palabra.isalpha()


def _cant_palabras_restantes(numero_palabras_requerido, palabras_ingresadas_validas):

    return numero_palabras_requerido - len(palabras_ingresadas_validas)


def interaccion_entero(minimo, maximo, mensaje):
    """Solicitar por consola un numero entero entre <minimo> y <maximo>
    (incluyendo ambos como valores permitidos), hasta que se ingrese un valor
    válido o se cancele la interacción ingresando SENTINELA.
    Si el usuario cancela la interacción, devolver interaccion.cancelada();
    si la interacción es exitosa, devolver interaccion.ok(valor ingresado)

    """

    mensaje += " < '{}' para cancelar >.".format(SENTINELA)
    mensaje += "\n"
    mensaje += " * Debe ser un número entre {0} y {1}.".format(minimo, maximo)
    mensaje += "\n"
    mensaje += ">> "

    numero_entero_valido = None
    while not numero_entero_valido:

        input_usuario = raw_input(mensaje)

        if input_usuario == SENTINELA:
            return interaccion.cancelar()

        if not input_usuario.isdigit():
            print "Inválido: '{}' no es un número.".format(input_usuario)
            continue

        entero_ingresado = int(input_usuario)
        if not (minimo <= entero_ingresado <= maximo):
            print "Inválido: {} no está entre {} y {}.".format(entero_ingresado, minimo, maximo)
            continue

        numero_entero_valido = entero_ingresado

    return interaccion.ok(numero_entero_valido)


def tamano_del_tablero(configuracion):
    return configuracion[INDICE_TAMANO_DEL_TABLERO]


def palabras(configuracion):
    return configuracion[INDICE_PALABRAS]
